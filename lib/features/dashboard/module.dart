import 'package:bloc_showcase/features/dashboard/presentation/bloc/bloc.dart';
import 'package:bloc_showcase/features/dashboard/presentation/pages/dashboard_page.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

final _presentationModule = [
  Provider(
    builder: (context) => DashboardBloc(),
    dispose: (_, bloc) => bloc.close(),
  )
];

class DashboardModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ..._presentationModule,
      ],
      child: DashboardPage(),
    );
  }
}
