import 'package:bloc_showcase/features/dashboard/domain/entities/dashboard_tab.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';

class DashboardState implements Equatable {
  final DashboardTab currentTab;

  bool get canGoBack => currentTab == DashboardTab.stateGraphBloc;

  DashboardState({@required this.currentTab});

  DashboardState copyWith({DashboardTab currentTab}) =>
      DashboardState(currentTab: currentTab ?? this.currentTab);

  @override
  List<Object> get props => [currentTab];
}
