import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';

abstract class DashboardEvent implements Equatable {}

class TabSelectedEvent extends DashboardEvent {
  final int tabIndex;
  final bool animate;

  TabSelectedEvent({@required this.tabIndex, @required this.animate});

  @override
  List<Object> get props => [tabIndex, animate];
}

class OnPopEvent extends DashboardEvent {
  @override
  List<Object> get props => null;
}
