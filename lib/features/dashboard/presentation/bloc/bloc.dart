import 'package:bloc_showcase/features/dashboard/domain/entities/dashboard_tab.dart';
import 'package:bloc_showcase/features/dashboard/presentation/bloc/state.dart';
import 'package:state_graph_bloc/state_graph_bloc.dart';

import 'event.dart';

class DashboardBloc extends StateGraphBloc<DashboardEvent, DashboardState> {
  DashboardBloc() {
    _currentPageSubject = singleLiveEventSubject();
    currentPage = _currentPageSubject.stream;
  }

  @override
  get initialState => DashboardState(currentTab: DashboardTab.stateGraphBloc);

  @override
  StateGraph<DashboardEvent, DashboardState> buildGraph() => StateGraph({
        DashboardState: {
          TabSelectedEvent: transitionWithSideEffect(
            (DashboardState state, TabSelectedEvent event) {
              final tabIndex = event.tabIndex;
              final newTab = DashboardTab.values[tabIndex];
              return state.copyWith(currentTab: newTab);
            },
            (_, TabSelectedEvent event) {
              if (event.animate) {
                _currentPageSubject.add(event.tabIndex);
              }
            },
          ),
          OnPopEvent: sideEffect(
              (state, _) => add(TabSelectedEvent(tabIndex: 0, animate: true)))
        },
      });

  SingleLiveEventSubject<int> _currentPageSubject;
  Stream<SingleLiveEvent<int>> currentPage;
}
