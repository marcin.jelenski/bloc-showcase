import 'dart:io';

import 'package:bloc_showcase/common/domain/exception/request_exception.dart';
import 'package:bloc_showcase/features/state_graph_bloc/domain/repositories/sample_repository.dart';
import 'package:http/http.dart';

abstract class LogInUseCase {
  Future<void> launch();
}

class LoginUseCaseImpl extends LogInUseCase {
  final SampleRepository userRepository;

  LoginUseCaseImpl(this.userRepository);

  @override
  Future<void> launch() async {
    try {
      await userRepository.signIn();
    } on ClientException catch (exc) {
      throw RequestError(RequestErrorCause.client, message: exc.message);
    } on SocketException {
      throw RequestError(
        RequestErrorCause.network,
        message: "No internet connection",
      );
    }
  }
}
