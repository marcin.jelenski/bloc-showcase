import 'package:bloc_showcase/features/account/domain/repositories/user_repository.dart';
import 'package:bloc_showcase/features/state_graph_bloc/presentation/pages/sample_page.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import 'data/datasources/typicode_data_source.dart';
import 'data/repositories/user_repository_impl.dart';
import 'domain/repositories/sample_repository.dart';
import 'domain/usecases/login_use_case.dart';
import 'presentation/bloc/bloc.dart';

final _domainModule = [
  ProxyProvider<SampleRepository, LogInUseCase>(
    builder: (context, repository, _) => LoginUseCaseImpl(repository),
  ),
];

final _dataModule = [
  Provider<TypicodeDataSource>(builder: (context) => TypicodeDataSource()),
  ProxyProvider<TypicodeDataSource, SampleRepository>(
    builder: (context, dataSource, _) => SampleRepositoryImpl(dataSource),
  ),
];

final _presentationModule = [
  ProxyProvider2<LogInUseCase, UserRepository, SampleBloc>(
    builder: (context, logInUseCase, userRepository, _) => SampleBloc(
      logInUseCase,
      userRepository,
    ),
    dispose: (_, bloc) => bloc.close(),
  ),
];

class StateGraphModule extends StatelessWidget {
  final String title;

  const StateGraphModule({Key key, @required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ..._dataModule,
        ..._domainModule,
        ..._presentationModule,
      ],
      child: SamplePage(title: title),
    );
  }
}
