import 'dart:core';

import 'package:bloc_showcase/common/domain/entities/snack_data.dart';
import 'package:bloc_showcase/features/state_graph_bloc/presentation/bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stream_subscription_builder/stream_subscription_builder.dart';

class SamplePage extends StatelessWidget {
  SamplePage({Key key, @required this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<SampleBloc>(context);

    return Scaffold(
      appBar: AppBar(
        title: StreamBuilder(
          stream: bloc.stateAsText(),
          builder: (context, snapshot) => Text("State: ${snapshot.data}"),
        ),
      ),
      body: StreamSubscriptionBuilder(
        (context) {
          final scaffold = Scaffold.of(context);

          return [
            bloc.showSnack.listen((event) => event.use(
                  (data) => scaffold.showSnackBar(_buildSnackBar(data)),
                )),
          ];
        },
        child: BlocBuilder<SampleBloc, SampleBlocState>(
          builder: (context, state) {
            if (state is InitialState) {
              return Center(
                child: MaterialButton(
                  onPressed: () => bloc.add(FetchEvent()),
                  child: Text("Fetch"),
                ),
              );
            } else if (state is LoadingState) {
              return Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    MaterialButton(
                      onPressed: () => bloc.add(CancelEvent()),
                      child: Text("Cancel"),
                    ),
                  ],
                ),
              );
            } else if (state is FetchedState) {
              return Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    MaterialButton(
                      onPressed: () => bloc.add(IncreaseEvent()),
                      child: Text("Increase"),
                    ),
                    MaterialButton(
                      onPressed: () => bloc.add(DecreaseEvent()),
                      child: Text("Decrease"),
                    ),
                    MaterialButton(
                      onPressed: () => bloc.add(RetryEvent()),
                      child: Text("Retry"),
                    ),
                    MaterialButton(
                      onPressed: () => bloc.add(LogoutEvent()),
                      child: Text("Logout"),
                    ),
                  ],
                ),
              );
            } else if (state is FetchErrorState) {
              return Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Error loading data"),
                    MaterialButton(
                      onPressed: () => bloc.add(RetryEvent()),
                      child: Text("Retry"),
                    ),
                  ],
                ),
              );
            }

            return Center();
          },
        ),
      ),
    );
  }

  SnackBar _buildSnackBar(SnackData data) {
    Color background;
    switch (data.status) {
      case SnackStatus.info:
        background = Colors.blue[700];
        break;
      case SnackStatus.success:
        background = Colors.green[700];
        break;
      case SnackStatus.warning:
        background = Colors.yellow[900];
        break;
      case SnackStatus.error:
        background = Colors.red[700];
        break;
    }

    return SnackBar(
      content: Text(data.text),
      duration: Duration(seconds: 3),
      backgroundColor: background,
    );
  }
}
