// Example of data source.
import 'package:http/http.dart';

class TypicodeDataSource {
  Future<Response> fetchTest() =>
      get("https://jsonplaceholder.typicode.com/posts");
}
