import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';

abstract class AccountCommonEvent with EquatableMixin {}

class SignInEvent extends AccountCommonEvent {
  final String username;

  SignInEvent({@required this.username});

  @override
  List<Object> get props => [username];
}

class SignOutEvent extends AccountCommonEvent {
  @override
  List<Object> get props => null;
}

class UserSignedEvent extends AccountCommonEvent {
  @override
  List<Object> get props => null;
}

class UserNotSignedEvent extends AccountCommonEvent {
  @override
  List<Object> get props => null;
}
