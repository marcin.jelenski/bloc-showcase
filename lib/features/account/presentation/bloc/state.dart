import 'package:equatable/equatable.dart';

abstract class AccountState with EquatableMixin {}

class LoadingState extends AccountState {
  @override
  List<Object> get props => null;
}

class SignedState extends AccountState {
  @override
  List<Object> get props => null;
}

class UnsignedState extends AccountState {
  @override
  List<Object> get props => null;
}
