import 'package:flutter/widgets.dart';
import 'package:regex_router/regex_router.dart';

import 'presentation/pages/regex_router_index_page.dart';
import 'presentation/pages/regex_router_sub_page.dart';

class RegexRouterModule extends StatelessWidget {
  final Key navigatorKey;

  const RegexRouterModule({Key key, this.navigatorKey}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final router = RegexRouter.create({
      "/": (context, _) => RegexRouterIndexPage(),
      "/second/:arg1": (context, args) => RegexRouterSubPage(
            first: args["arg1"],
            body: args.body,
          ),
      "/second/:arg1/:arg2": (context, args) => RegexRouterSubPage(
            first: args["arg1"],
            second: args["arg2"],
            body: args.body,
          ),
    });

    return Navigator(
      key: navigatorKey,
      initialRoute: "/",
      onGenerateRoute: router.generateRoute,
    );
  }
}
