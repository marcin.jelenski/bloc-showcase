import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class RegexRouterSubPage extends StatelessWidget {
  RegexRouterSubPage({Key key, @required this.first, this.second, this.body})
      : super(key: key);

  final String first;
  final String second;
  final Object body;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Second screen'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text("Route first argument: $first"),
            Text("Route second argument: $second"),
            Text("Body argument: $body"),
          ],
        ),
      ),
    );
  }
}
