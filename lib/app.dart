import 'package:bloc_showcase/features/account/module.dart';
import 'package:bloc_showcase/features/account/presentation/bloc/event.dart';
import 'package:bloc_showcase/features/dashboard/module.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:regex_router/regex_router.dart';

import 'features/account/presentation/bloc/bloc.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AccountModule(
      loadingScope: (_) => Container(
        color: Colors.blue,
      ),
      signedScope: (_) => _buildSignedScope(),
      unsignedScope: _buildUnsignedScope,
    );
  }

  MaterialApp _buildSignedScope() {
    final router = RegexRouter.create({
      "/": (context, _) => DashboardModule(),
    });

    return MaterialApp(
      key: Key("signed"),
      onGenerateRoute: router.generateRoute,
      initialRoute: "/",
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    );
  }

  MaterialApp _buildUnsignedScope(BuildContext context) {
    final bloc = BlocProvider.of<AccountBloc>(context);

    final router = RegexRouter.create({
      "/": (context, _) => Scaffold(
            body: MaterialButton(
              child: Center(child: Text("Sign!")),
              onPressed: () =>
                  bloc.add(SignInEvent(username: "me@example.com")),
            ),
          ),
    });

    return MaterialApp(
      key: Key("unsigned"),
      onGenerateRoute: router.generateRoute,
      initialRoute: "/",
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    );
  }
}
