class RequestError implements Exception {
  final RequestErrorCause cause;
  final String message;

  RequestError(this.cause, {this.message});
}

enum RequestErrorCause {
  client,
  network,
}
