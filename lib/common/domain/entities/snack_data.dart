class SnackData {
  final String text;
  final SnackStatus status;

  SnackData(this.text, this.status);
}

enum SnackStatus {
  info,
  success,
  warning,
  error,
}
